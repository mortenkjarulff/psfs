#!/usr/bin/env python3

# A very simple readonly FUSE filesystem on top of pass (https://passwordstore.org).

# If mounted at ~/.psfs, these 2 commands should give the same output:
#   pass mysecret
#   cat ~/.psfs/mysecret?show

# Start:
#   mkdir ~/.psfs
#   ./psfs.py ~/.psfs

# Stop:
#   fusermount -u ~/.psfs

# Test:
#   echo secret >mysecret
#   cat mysecret # THIS
#   cat mysecret | pass insert -m mysecret
#   rm mysecret
#   ln -s ~/.psfs/mysecret?show mysecret
#   cat mysecret # AND THIS should show the same

# Tested on Pop!_OS.
# Tested on Ubuntu on WSL.

# To get started I looked at:
#   https://github.com/danielrozenberg/httpfs/blob/master/httpfs.py
#   https://github.com/skorokithakis/python-fuse-sample/blob/master/passthrough.py

# Comments are very wellcome!

from sys import argv
from fusepy import FUSE, FuseOSError, Operations
from stat import S_IFDIR, S_IFREG
from errno import EIO
from subprocess import run

class psfs(Operations):

  def getattr(self, path, fh):
    if not path.endswith('?show'):
      return dict(st_mode=(S_IFDIR | 0o500))
    return dict(st_mode=(S_IFREG | 0o400), st_size=128*256*256*256)

  def read(self, path, size, offset, fh):
    if not path.endswith('?show'):
      raise FuseOSError(EIO)
    data=run('pass %s' % (path[:-5]), capture_output=True, shell=True)
    if data.returncode != 0:
      raise FuseOSError(EIO)
    if data.stderr != b'':
      raise FuseOSError(EIO)
    return data.stdout
 
if __name__ == '__main__':
  FUSE(psfs(), argv[1], nothreads=True, foreground=True, allow_other=False)
